﻿using BuzzFeed.Data;
using BuzzFeed.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BuzzFeed.Controllers
{
    public class CategoryController : Controller
    {
        CategoryData _category;
        PostData _post;

        public CategoryController()
        {
            _category = new CategoryData();
            _post = new PostData();
        }

        public ActionResult Index(string slug, int page = 1)
        {
            var category = _category.GetBy(x => x.Slug == slug).FirstOrDefault();

            var posts = _post.GetByPage(x => x.CategoryId == category.Id && x.IsActive && !x.IsDelete, page, 15, "BuzzId", true);
            var totalCount = _post.GetCount(x => x.CategoryId == category.Id && x.IsActive && !x.IsDelete);

            if (posts != null)
            {
                foreach (var item in posts)
                {
                    item.Category = _category.GetByKey(item.Id);
                }
            }

            var model = new CategoryViewModel()
            {
                Category = category,
                Posts = posts,
                Current = page,
                PageCount = (int)Math.Ceiling(totalCount / (decimal)10)
            };

            return View(model);
        }
    }
}