﻿namespace BuzzFeed.Data
{
    using BuzzFeed.Data.Core;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class ContentData : Core.EntityBaseData<Model.Content>
    {
        public ContentData() : base(new DataContext())
        {

        }
    }
}