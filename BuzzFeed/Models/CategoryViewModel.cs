﻿using BuzzFeed.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuzzFeed.Models
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
            Posts = new List<Post>();
        }

        public Category Category { get; set; }
        public List<Post> Posts { get; set; }
        public int Current { get; set; }
        public int PageCount { get; set; }
    }
}