﻿namespace BuzzFeed.Data
{
    using BuzzFeed.Data.Core;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class PostData : Core.EntityBaseData<Model.Post>
    {
        public PostData() : base(new DataContext())
        {

        }
    }
}