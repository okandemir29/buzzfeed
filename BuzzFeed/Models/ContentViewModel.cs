﻿using BuzzFeed.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuzzFeed.Models
{
    public class ContentViewModel
    {
        public ContentViewModel()
        {
            Contents = new List<Content>();
        }

        public Post Post { get; set; }
        public Bundle Bundle { get; set; }
        public List<Post> RelatedPosts { get; set; }
        public List<Content> Contents { get; set; }
    }
}