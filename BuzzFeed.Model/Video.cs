﻿namespace BuzzFeed.Model
{
    using System;

    public class Video : Core.ModelBase
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string CoverPath { get; set; }
        public string VideoPath { get; set; }
        public string Type { get; set; }
        public string Slug { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }
}
