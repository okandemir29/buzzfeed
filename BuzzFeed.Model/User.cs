﻿namespace BuzzFeed.Model
{
    using System;

    public class User : Core.ModelBase
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
    }
}
