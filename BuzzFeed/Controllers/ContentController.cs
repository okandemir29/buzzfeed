﻿using BuzzFeed.Data;
using BuzzFeed.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace BuzzFeed.Controllers
{
    public class ContentController : Controller
    {
        PostData _post;
        BundleData _bundle;
        CategoryData _category;
        ContentData _content;

        public ContentController()
        {
            _post = new PostData();
            _category = new CategoryData();
            _content = new ContentData();
            _bundle = new BundleData();
        }

        public ActionResult Index(string slug)
        {
            if (string.IsNullOrEmpty(slug))
                return RedirectToAction("Index", "Home", new { t = "no_content" });

            var detail = _post.GetBy(x => x.Slug == slug).FirstOrDefault();

            if (detail != null)
            {
                detail.Hit++;
                var result = _post.Update(detail);

                var model = new ContentViewModel()
                {
                    Post = detail,
                    RelatedPosts = _post.GetRandom(3),
                    Contents = _content.GetBy(x => x.PostId == detail.BuzzId),
                };

                model.Post.Category = _category.GetBy(x => x.Id == model.Post.CategoryId).FirstOrDefault();

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home", new { t = "no_content" });
        }

        public ActionResult Bundle(string slug, string type)
        {
            if (string.IsNullOrEmpty(slug) && string.IsNullOrEmpty(type))
                return RedirectToAction("Index", "Home", new { t = "no_content" });

            var detail = _bundle.GetBy(x => x.Slug == slug).FirstOrDefault();

            if (detail != null)
            {
                var model = new ContentViewModel()
                {
                    Bundle = detail,
                    RelatedPosts = _post.GetRandom(3),
                };

                model.Bundle.Category = _category.GetBy(x => x.Id == model.Bundle.CategoryId).FirstOrDefault();

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home", new { t = "no_content" });
        }
    }
}