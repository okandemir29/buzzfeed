﻿namespace BuzzFeed.Data
{
    using BuzzFeed.Data.Core;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class BundleData : Core.EntityBaseData<Model.Bundle>
    {
        public BundleData() : base(new DataContext())
        {

        }
    }
}