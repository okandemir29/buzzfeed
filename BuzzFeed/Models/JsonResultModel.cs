﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuzzFeed.Models
{
    public class JsonResultModel
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("response")]
        public string Response { get; set; }

        [JsonProperty("api_requests_made")]
        public long ApiRequestsMade { get; set; }

        [JsonProperty("api_requests_available")]
        public long ApiRequestsAvailable { get; set; }

        [JsonProperty("protected_terms")]
        public string ProtectedTerms { get; set; }

        [JsonProperty("nested_spintax")]
        public bool NestedSpintax { get; set; }

        [JsonProperty("confidence_level")]
        public string ConfidenceLevel { get; set; }
    }
}