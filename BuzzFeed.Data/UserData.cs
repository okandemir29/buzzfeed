﻿namespace BuzzFeed.Data
{
    using BuzzFeed.Data.Core;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class UserData : Core.EntityBaseData<Model.User>
    {
        public UserData() : base(new DataContext())
        {

        }
    }
}