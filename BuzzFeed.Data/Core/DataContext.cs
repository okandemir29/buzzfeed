﻿namespace BuzzFeed.Data.Core
{
    using System.Data.Entity;
    using BuzzFeed.Model;
    public class DataContext : DbContext
    {
        public DbSet<Content> Contents { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Bundle> Bundles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Content>().ToTable("Content");
            modelBuilder.Entity<Category>().ToTable("Category");
            modelBuilder.Entity<Video>().ToTable("Video");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Post>().ToTable("Post");
            modelBuilder.Entity<Bundle>().ToTable("Bundle");
            base.OnModelCreating(modelBuilder);
        }
    }
}