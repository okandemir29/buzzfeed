﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BuzzFeed
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "CategoryPage",
                url: "category/{slug}/page/{page}",
                defaults: new { controller = "Category", action = "Index" }
            );

            routes.MapRoute(
                name: "Category",
                url: "category/{slug}",
                defaults: new { controller = "Category", action = "Index" }
            );

            routes.MapRoute(
                name: "Content",
                url: "{slug}",
                defaults: new { controller = "Content", action = "Index" }
            );

            routes.MapRoute(
                name: "Bundle",
                url: "{slug}/{type}",
                defaults: new { controller = "Content", action = "Bundle" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
