﻿using BuzzFeed.Data;
using BuzzFeed.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace BuzzFeed.Helpers
{
    public static class CacheHelper
    {

        public static List<Post> HeadLine
        {
            get
            {
                if (HttpContext.Current.Cache["Banner"] == null)
                {
                    var post = new PostData().GetBy(x=>x.IsActive && !x.IsDelete).OrderByDescending(x=>x.BuzzId).Take(3).ToList();

                    HttpContext.Current.Cache.Add(
                        "Banner"
                        , post
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null
                    );
                }
                return HttpContext.Current.Cache["Banner"] as List<Post>;
            }
        }
        
        public static List<Category> Categories
        {
            get
            {
                if (HttpContext.Current.Cache["Categories"] == null)
                {
                    var post = new CategoryData().GetAll();

                    HttpContext.Current.Cache.Add(
                        "Categories"
                        , post
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null
                    );
                }
                return HttpContext.Current.Cache["Categories"] as List<Category>;
            }
        }

        public static List<Post> SideBarRandom
        {
            get
            {
                if (HttpContext.Current.Cache["SideBarRandom"] == null)
                {
                    var post = new PostData().GetRandom(5);

                    HttpContext.Current.Cache.Add(
                        "SideBarRandom"
                        , post
                        , null
                        , DateTime.Now.AddDays(1)
                        , Cache.NoSlidingExpiration
                        , CacheItemPriority.AboveNormal
                        , null
                    );
                }
                return HttpContext.Current.Cache["SideBarRandom"] as List<Post>;
            }
        }


    }
}