﻿namespace BuzzFeed.Model
{
    using System;

    public class Content : Core.ModelBase
    {
        public string Header { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public string VideoPath { get; set; }
        public string Youtube { get; set; }
        public string Tumblr { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }

        public int PostId { get; set; }
        public virtual Post Post { get; set; }
    }
}
