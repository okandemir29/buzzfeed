﻿namespace BuzzFeed.Data
{
    using BuzzFeed.Data.Core;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class CategoryData : Core.EntityBaseData<Model.Category>
    {
        public CategoryData() : base(new DataContext())
        {

        }
    }
}