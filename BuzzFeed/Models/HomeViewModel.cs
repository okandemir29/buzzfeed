﻿using BuzzFeed.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuzzFeed.Models
{
    public class HomeViewModel
    {
        public HomeViewModel()
        {
            Posts = new List<Post>();
            Videos = new List<Video>();
            Bundles = new List<Bundle>();
        }

        public List<Post> Posts { get; set; }
        public List<Video> Videos { get; set; }
        public List<Bundle> Bundles { get; set; }
        public int Current { get; set; }
        public int PageCount { get; set; }
    }
}