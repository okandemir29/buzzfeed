﻿using BuzzFeed.Data;
using BuzzFeed.Model;
using BuzzFeed.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace BuzzFeed.Controllers
{
    public class HomeController : Controller
    {
        ContentData _content;
        PostData _post;
        CategoryData _category;
        VideoData _video;
        BundleData _bundle;

        public HomeController()
        {
            _content = new ContentData();
            _category = new CategoryData();
            _post = new PostData();
            _video = new VideoData();
            _bundle = new BundleData();
        }
        public ActionResult Index()
        {
            var posts = _post.GetBy(x => x.IsActive && !x.IsDelete).OrderByDescending(x => x.BuzzId).Take(50).ToList();
            var videos = _video.GetRandom(x=>x.IsActive && !x.IsDelete, 5);
            var bundles = _bundle.GetBy(x => x.IsActive).ToList();

            var model = new HomeViewModel
            {
                Posts = posts,
                Videos = videos,
                Bundles = bundles,
            };

            if (model.Posts != null && model.Posts.Count > 0)
            {
                foreach (var item in model.Posts)
                {
                    item.Category = _category.GetByKey(item.CategoryId);
                }
            }

            if (model.Bundles != null && model.Bundles.Count > 0)
            {
                foreach (var item in model.Bundles)
                {
                    item.Category = _category.GetByKey(item.CategoryId);
                }
            }

            return View(model);
        }

        public ActionResult Duzelt()
        {
            var datas = _content.GetAll();

            foreach (var item in datas)
            {
                if (item.Description != null)
                {
                    item.Description = item.Description.Replace("â€™", "'");
                    item.Description = item.Description.Replace("Ã±", "ñ").Replace("â€”", " ").Replace("â€", "").Replace("â€œ","");
                    var guncel = _content.Update(item);
                }
            }

            return RedirectToAction("Index", "Home", new { s = "Icerik-Duzeltildi" });
        }
    }
}