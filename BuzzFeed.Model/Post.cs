﻿namespace BuzzFeed.Model
{
    using System;

    public class Post : Core.ModelBase
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string LittleImagePath { get; set; }
        public string BigImagePath { get; set; }
        public string Slug { get; set; }
        public int BuzzId { get; set; }
        public int Hit { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
