﻿namespace BuzzFeed.Data
{
    using BuzzFeed.Data.Core;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class VideoData : Core.EntityBaseData<Model.Video>
    {
        public VideoData() : base(new DataContext())
        {

        }
    }
}