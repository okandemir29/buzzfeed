﻿using System;

namespace BuzzFeed.Model.Core
{
    public class IgnoreAttribute : Attribute
    {
        public string SomeProperty { get; set; }
    }
}
